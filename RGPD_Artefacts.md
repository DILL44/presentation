# Présentation de la RGPD
RGPD Réglementation Général sur la Protection des Données   
Beaucoup d'éléments étaient déjà présents dans la loi de 1978 Française sur la protection des données


# Objectif de la journée
* Comprendre l'esprit du RGPD
* Identifier notre responsabilité
* Envisager les procédures internes
* Déterminer les chantiers


### Définition d'une donnée personnelle
[site de la cnil](https://www.cnil.fr/fr/definition/donnee-personnelle)   
"Toute information identifiant directement ou indirectement une personne physique (ex. nom, no d’immatriculation, no de téléphone, photographie, date de naissance, commune de résidence, empreinte digitale...)."


### Définition d'une donnée personnelle (2)
* C'est un email, un nom, une adresse IP ...
* C'est présent dans un fichier clients, les logs d'un serveur, des datas pour faire de l'analyse de trafic ...
* Elle a le droit d'être collectée avec un consentement explicite de l'utilisateur
* On doit se poser 2 questions pour pouvoir justifier la collecte de données: A quoi elle sert? combien de temps on en fait usage?


### Droit individuel
* "Privacy by design" (le non consentement ne peux pas bloquer l’accès au service)   
* [Tartre au citron](https://opt-out.ferank.eu/fr/install/): un outils pour le consentement des services extérieurs   
* [Droit à l'oubli](https://www.cnil.fr/fr/le-dereferencement-dun-contenu-dans-un-moteur-de-recherche)


### différence Buisness et client
* la RGPD ne s'applique pas au collecte de données individuelles
* [Emailing](https://www.cnil.fr/fr/la-prospection-commerciale-par-courrier-electronique): B2B obligation d'information, B2C obligation du consentement


### Outils statistiques
* Là encore il faudra le consentement des utilisateurs sauf avec [Matomo (ancienement piwik) et xiti](https://www.cnil.fr/fr/solutions-pour-les-cookies-de-mesure-daudience).   
* Pour Google Analytics ils ont envoyé des mails de mise en conformité ...    
En tant que propriétaire de la donnée vous devez vérifier que votre sous-traitant est en conformité


### Principe de portabilité des données
[Possibilité de demander ses données](https://www.cnil.fr/fr/le-droit-la-portabilite-en-questions) personnelles pour pouvoir les réimporter dans un autre service de même type


### La sous-traitance
* Les sous-traitants ont un devoir de conseil sur la conformité   
* Contractualisation des problématiques de procédure des données   
* En cas de problème, la responsabilité peut être partagée entre le sous-traitant et le propriétaire
* [guide du sous-traitant](https://www.cnil.fr/sites/default/files/atoms/files/rgpd-guide_sous-traitant-cnil.pdf)


### Contractualisation entre responsable de la donnée et sous-traitant
* objet et durée de la collecte + détail des données perso traitées
* les bases juridiques : consentement des personnes, intérêt légitime, contrat, obligation légale


### Contractualisation entre responsable de la donnée et sous-traitant (2)
* références légales
* obligation des sous-traitants
* mesures de sécurité mises en place : techniques et code de bonnes conduites


### Échange de données en toutes sécurité
* Pour la sous-traitance on peut utiliser la pseudonimisation   
* Pour de la publication on peut utiliser l'anonymisation (au moins 8 entrées identiques)


### Obligation de documenter sa mise en conformité
* DPO, travail : interlocuteur avec la CNIL   
* Mettre en place des procédures
* Code de bonnes conduites (bonnes pratiques internes)
* Traçabilité / sécurisation   


### Documents à avoir
* Le registre des données
* Les analyses d'impact sur la protection des données : cf. PIA
* Information aux personnes: modèles de recueil de consentement et procédure pour informer sur exercice des droits
* Contrats types


### Documents à avoir (2)
* Doc sur procédures internes si violation
* Preuves : autorisation des personnes, traçabilité des données, informations au responsable des données ou sous-traitant (selon qui on est), procédures internes pour règles de bonnes conduites


### Je constate une fuite de données
Vous avez 72 heures pour prévenir la CNIL et les personnes qui peuvent en être impactées


### Les sanctions
La CNIL prévoit un souplesse de 3 ans pour la mise en conformité  
* 20 000 000 d’euros ou
* 4% du chiffre d’affaires mondial
Responsabilité répartie en pourcentage -> Somme amandée répartie au prorata


### Le cadre d’application
* Toute entreprise/association/institution Européen   
* Toute utilisateur qui est en Europe   
* ET toute entreprise qui traite des données d'européens


### la possibilité d’action collective
La quadrature du net va attaquer les GAFAM le 25 mai


### Attention beaucoup d'arnaque sur la RGPD
Une bible pour la RGPD : le site de la CNIL   
sources internes:
* [wiki](http://documentation.artefacts.coop/index.php?title=RGPD)
* [pad](https://mypads.framapad.org/mypads/?/mypads/group/rgpd-mgw7t7zv/pad/view/rgpd-0lw8t7mk)
* [framateam](https://framateam.org/artefacts/channels/rgpd)



# Un petit atelier


### Des quelques exemples concrets
* Le site Internet des promeneurs du net
* Le recensement de la filière Images en mouvement
* La fiche d'adhésion à PiNG
* Les infos des entrepreneurs au sein d'Artefacts
* La maintenance d'une plateforme numérique
* La communication web d'une boutique éphémère


### Des quelques exemples concrets (2)
Pour ces exemples, en petits groupes :
* Identifier si on est responsable de la donnée ou sous-traitant
* Lister les données personnelles
* En quoi la donnée est sensible
* Process a mettre en œuvre (traçabilité, qui manipule, mesure de sécurité, bonnes pratiques)
* preuves à fournir


### Quelles solutions
On reprend les idées listées pèle-mêle :
   * Le registre des données
   * Les analyses d'impact sur la protection des données : cf. PIA
   * DPO ?
   * Information aux personnes : modèles de recueil de consentement et procédures pour informer sur exercice des droits
   * Contrats types


### Quelles solutions (2)
   * Doc sur procédures internes si violation
   * Preuves : autorisation des personnes, traçabilité des données, informations au responsable des données ou sous-traitant (selon qui on est), procédures internes pour règles de bonnes conduites, conditions de travail du dpo, stockage
   * Process : à définir, formation socle, coffre fort numérique ...


### Feuille de route
* Qu'est ce qu'il y a a faire ?
* Comment on s’organise ?
