# Présentation de la RGPD
RGPD réglementation sur la protection des données   
Beaucoup d'éléments étaient déjà présent dans la loi Française sur la protection des données


### Définition d'une donnée personnelle
[site de la cnil](https://www.cnil.fr/fr/definition/donnee-personnelle)   
"Toute information identifiant directement ou indirectement une personne physique (ex. nom, no d’immatriculation, no de téléphone, photographie, date de naissance, commune de résidence, empreinte digitale...)."


### Définition d'une donnée personnelle (2)
* C'est un fichier clients, les logs d'un serveur, des datas pour faire de l'analyse de trafic.   
* Elle a le droit d'être collecté avec un consentement explicite de l'utilisateur
* On doit ce poser 2 questions pour pouvoir justifier la collecte de données: A quoi elle sert? combien de temps elle sert?


### Droit individuel
* "Privacy by design" (le non consentement ne peux pas bloquer l’accès au service)   
* Tartre au citron un outils pour le consentement des services extérieurs   
* Droit à l'oublie


### Outils statistiques
* Là encore il faudra le consentement des utilisateurs sauf avec Matomo (ancienement piwik) et xiti.   
* Pour Google Analytics il ont envoyer des mail de mise en conformité ...    
en tant que propriétaire de la donnée vous devez vérifier que votre sous-traitant est en conformité


### Principe de portabilité des données
Possibilité de demandé ces données personnelles pour pouvoir les réimporter dans un autre service de même type


### La sous-traitance
* Les sous-traitant ont un devoir de conseil sur la conformité   
* Contractualisation des problématiques de procédure des données   
* En cas de problème la responsabilité peut-être partager entre le sous-traitant et le propriétaire


### Échange de données en toutes sécurité
* Pour la sous-traitance on peut utiliser la pseudonimisation   
* Pour de la publication on peut utiliser l'anonymisation (au moins 8 entrés identiques)


### Obligation de documenter sa mise en conformité
* PIA outils de la CNIL pour cartographier nos données   
* Traçabilité / sécurisation   
* DPO, travail : cartographier nos données   
* Mettre en place des procédures


### J'ai eu une intrusion
Vous avez 72 heures pour prévenir la CNIL et les personnes qui peuvent en être impactées


### Les sanctions
la CNIL ne va pas tomber sur tous les gens qui ne sont pas en conformité mais si elle voit que vous êtes de bonne volonté elle va vous accompagné   
* 20 000 000 d’euros ou
* 4% du chiffre d’affaires mondial


### Le cadre d’application
* Toute entreprise/association/institution Européen   
* Toute utilisateur qui est en Europe   
* ET toute entreprise qui traite des données d'européens


### la possibilité d’action collective
La quadrature du net va attaqué les GAFAM demain


### Attention beaucoup d'arnaque sur la RGPD
Une bible pour la RGPD : le site de la CNIL



# Un petit atelier


### des quelques exemples concrets
   * Le site Internet d'une association
   * La fiche d'adhésion à PiNG
   * La maintenance d'une plateforme numérique
   * La communication web d'une boutique éphémère


### des quelques exemples concrets (2)
Pour ces exemples, en petits groupes :
   * Identifier si on est responsable de la donnée ou sous-traitant
   * Lister les données personnelles


### Quelles solutions
On reprend les idées listées pèle-mêle sur le paperboard, que l'on hierarchise, en rebalançant
   * Le registre des données
   * Les analyses d'impact sur la protection des données : cf. PIA
   * DPO?
   * Information aux personnes: modèles de recueil de consentement et procédure pour informer sur exercice des droits
   * Contrats types


### Quelles solutions (2)
   * Doc sur procédures internes si violation
   * Preuves : autorisation des personnes, traçabilité des données, informations au responsable des données ou sous-traitant (selon qui on est), procédures internes pour règles de bonnes conduites, conditions de travail du dpo, stockage
   * Process : à définir, formation socle, coffre fort numérique,
